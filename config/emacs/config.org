#+TITLE: Emacs Configuration
#+AUTHOR: Elarna
#+OPTIONS: toc:1
#+STARTUP: content

* Packages

** Management

Use straight.el for package management.

#+begin_src emacs-lisp
  (defvar bootstrap-version)
  (let ((bootstrap-file
         (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
        (bootstrap-version 5))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer
          (url-retrieve-synchronously
           "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
           'silent 'inhibit-cookies)
        (goto-char (point-max))
        (eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage))
#+end_src

And use use-package for package configuration.

#+begin_src emacs-lisp
  (straight-use-package 'use-package)
#+end_src

** Completion

Company for text completion, enabled only in programming modes.

#+begin_src emacs-lisp
  (use-package company
    :straight t
    :hook (prog-mode . company-mode)
    :bind
    (:map company-active-map
          ("C-n" . company-select-next)
          ("C-p" . company-select-previous)))
#+end_src

Vertico, Consult, and Orderless for improved minibuffer completion, making it easy to find files, search files, navigate buffers, etc. Used in combination with save-hist for persistent minibuffer history.

#+begin_src emacs-lisp
  (use-package consult
    :straight t
    :bind
    ("C-s" . consult-line)
    ("C-c i" . consult-imenu)
    ("C-x b" . consult-buffer)
    ("C-c ! 1" . consult-flymake)
    ("C-c y" . consult-yank-from-kill-ring))

  (use-package orderless
    :straight t
    :custom (completion-styles '(orderless)))

  (use-package savehist
    :init (savehist-mode))

  (use-package vertico
    :straight t
    :init (vertico-mode)
    :custom (vertico-cycle t))
#+end_src

** Language Server Protocol

Eglot as my LSP client of choice.

#+begin_src emacs-lisp
  (use-package eglot
    :straight t
    :hook
    (c-mode . eglot-ensure)
    (sh-mode . eglot-ensure)
    (python-mode . eglot-ensure))
#+end_src

** Correction

Flymake for syntax checking.

#+begin_src emacs-lisp
  (use-package flymake
    :straight (:type built-in)
    :hook (prog-mode . flymake-mode))

  (use-package flymake-shellcheck
    :straight t
    :commands flymake-shellcheck-load
    :hook (sh-mode . flymake-shellcheck-load))
#+end_src

Flyspell for spell checking in text-based modes.

#+begin_src emacs-lisp
  (use-package flyspell
    :straight (:type built-in)
    :hook
    (org-mode . flyspell-mode)
    (text-mode . flyspell-mode))
#+end_src

** Dired

Configure dired to be less busy and enable dired-jump to easily find files in the current working directory.

#+begin_src emacs-lisp
  (use-package dired
    :straight (:type built-in)
    :commands (dired dired-jump)
    :bind ("C-x C-j" . dired-jump)
    :hook (dired-mode . dired-hide-details-mode)
    :custom (dired-listing-switches "-lah  --group-directories-first"))
#+end_src

Ibuffer for management of multiple buffers in the same manner as dired.

#+begin_src emacs-lisp
  (use-package ibuffer
    :straight (:type built-in)
    :bind ("C-x C-b" . ibuffer))
#+end_src

** Org

In Org-mode, align text lines (that are not headlines) with headline text and disable automatic re-indentation.

#+begin_src emacs-lisp
  (use-package org
    :straight (:type built-in)
    :hook
    (org-mode . (lambda ()
                  (org-indent-mode)
                  (electric-indent-local-mode -1))))
#+end_src

** Music

EMMS so that Emacs can be used as my default music player. Also enable notifications so I can be notified of track changes.

#+begin_src emacs-lisp
  (use-package emms
    :straight t
    :config (emms-all)
    :bind ("C-c e" . emms-smart-browse)
    :custom
    (emms-player-list '(emms-player-mpv))
    (emms-source-file-default-directory "~/Music/"))

  (use-package notifications
    :straight (:type built-in))
#+end_src

** IRC

My IRC login credentials are kept in a separate file which needs to be loaded before it can be used.

#+begin_src emacs-lisp
  (load-file "~/.config/emacs/.private.el")
#+end_src

I'm currently using Circe as my IRC client. I briefly experimented with ERC (another, slightly more feature-rich Emacs IRC client) but for now Circe is meeting my needs.

#+begin_src emacs-lisp
  (use-package circe
    :straight t
    :bind
    ("C-c I" . (lambda ()
                 (interactive)
                 (circe "ZNC")))
    :hook
    (circe-mode . (lambda ()
                    (circe-prompt)
                    (org-indent-mode)
                    (setq line-spacing 5)))
    :config
    (enable-circe-color-nicks)
    (set-face-attribute 'circe-originator-face nil :weight 'bold)
    :custom
    (circe-reduce-lurker-spam t)
    (circe-use-cycle-completion t)
    (circe-color-nicks-everywhere t)
    (circe-default-part-message nil)
    (circe-default-quit-message nil)
    (circe-chat-buffer-name "{target}")
    (circe-format-say "<{nick}> {body}")
    (circe-format-self-say "<{nick}> {body}")
    (circe-network-options
     `(("ZNC"
        :host "192.168.1.10"
        :port 6006
        :family ipv4
        :user ,znc-user
        :pass ,znc-pass))))
#+end_src

UI changes, such as neatly aligning all messages 13 spaces from the left.

#+begin_src emacs-lisp
  (use-package lui
    :after circe
    :hook (lui-pre-output . truncate-nicks)
    :custom
    (lui-fill-type 13)
    (lui-fill-column 72)
    (lui-time-stamp-format " %H:%M"))
#+end_src

Update the prompt so that it's in-line with the messages column.

#+begin_src emacs-lisp
  (defun circe-prompt ()
    (lui-set-prompt (concat (make-string 10 32)
                            (propertize ">>" 'face 'circe-prompt-face)
                            " ")))
#+end_src

Shorten long nicknames.

#+begin_src emacs-lisp
  (defun truncate-nicks ()
    (when-let (start (text-property-any (point-min) (point-max)
                                        'lui-format-argument 'nick))
      (goto-char start)
      (let ((end (next-single-property-change start 'lui-format-argument))
            (nick (plist-get (plist-get (text-properties-at start)
                                        'lui-keywords) :nick)))
        (when (>= (length nick) (- lui-fill-type 2))
          (compose-region (+ start (- lui-fill-type 2) -2) end "…")))))
#+end_src

* Appearance

** Start-Up

Disable unnecessary start-up messages.

#+begin_src emacs-lisp
  (setq inhibit-startup-message t)
  (setq initial-scratch-message nil)
#+end_src

** Theme

#+begin_src emacs-lisp
  (load-theme 'adwaita t)
#+end_src

** Font

#+begin_src emacs-lisp
  (add-to-list 'default-frame-alist '(font . "Ubuntu Mono 12" ))
#+end_src

** Frame Title

Make the title of the Emacs frame more informative.

#+begin_src emacs-lisp
  (setq-default frame-title-format '("Emacs: %b"))
#+end_src

** GUI

Disable graphical elements for a cleaner interface.

#+begin_src emacs-lisp
  (tooltip-mode 0)
  (menu-bar-mode 0)
  (tool-bar-mode 0)
  (scroll-bar-mode 0)
  (set-face-attribute 'fringe nil :background nil)
#+end_src

Add padding to the window.

#+begin_src emacs-lisp
  (add-to-list 'default-frame-alist '(internal-border-width . 20))
#+end_src

* Behavior

** Programming

Always show matching parenthesis.

#+begin_src emacs-lisp
  (show-paren-mode)
#+end_src

Never indent with tabs.

#+begin_src emacs-lisp
  (setq-default indent-tabs-mode nil)
#+end_src

Use the K&R indentation style for C.

#+begin_src emacs-lisp
  (setq c-default-style "k&r")
#+end_src

Delete trailing white-space on save.

#+begin_src emacs-lisp
  (add-hook 'before-save-hook 'delete-trailing-whitespace)
#+end_src

** Text

Enable line wrapping in text-based modes.

#+begin_src emacs-lisp
  (add-hook 'org-mode-hook 'turn-on-visual-line-mode)

  (add-hook 'text-mode-hook 'turn-on-visual-line-mode)
#+end_src

** General

Move backup files to a single directory.

#+begin_src emacs-lisp
  (setq backup-directory-alist
        `(("." . ,(concat user-emacs-directory "backups"))))
#+end_src

Prevent custom-set-variables and custom-set-faces from cluttering init.

#+begin_src emacs-lisp
  (setq custom-file (concat user-emacs-directory "custom.el"))
#+end_src

Make confirmation easier.

#+begin_src emacs-lisp
  (defalias 'yes-or-no-p 'y-or-n-p)
#+end_src

Save your ears.

#+begin_src emacs-lisp
  (setq ring-bell-function 'ignore)
#+end_src

* Functions

** Eshell

Toggle Eshell in a horizontally split window.

#+begin_src emacs-lisp
  (defun toggle-eshell ()
    (interactive)
    (if (get-buffer "*eshell*")
        (progn
          (kill-buffer "*eshell*")
          (delete-window))
      (split-window-below)
      (other-window 1)
      (eshell)))

  (global-set-key (kbd "C-t") 'toggle-eshell)
#+end_src

** Now-Playing

Display a system notification when EMMS selects a new track.

#+begin_src emacs-lisp
  (defun now-playing ()
    (let ((track (emms-track-get (emms-playlist-current-selected-track) 'info-title))
          (artist (emms-track-get (emms-playlist-current-selected-track) 'info-artist))
          (dir (file-name-directory (emms-track-name (emms-playlist-current-selected-track)))))
      (notifications-notify
       :replaces-id 36227
       :title "Now Playing"
       :body (concat track "\n" artist)
       :app-icon (concat dir "cover.jpg"))))

  (add-hook 'emms-playlist-selection-changed-hook #'now-playing)
#+end_src

* Mode-Line

** Time

Enable the display of time in the mode-line and configure its format.

#+begin_src emacs-lisp
  (setq display-time-string-forms '("[" 24-hours ":" minutes "]"))

  (setq display-time-interval 60)

  (display-time)
#+end_src

** Alignment

Calculate spacing for right-aligned items.

#+begin_src emacs-lisp
  (defun mode-line-spacing ()
    (propertize " "
                'display `((space :align-to
                                  (- (+ right right-fringe right-margin), 8)))))
#+end_src

** Face Attributes

Set different font weights for the active and inactive mode-lines and remove their borders.

#+begin_src emacs-lisp
  (set-face-attribute 'mode-line nil
                      :box nil
                      :weight 'bold)

  (set-face-attribute 'mode-line-inactive nil
                      :box nil
                      :weight 'normal)
#+end_src

** Formatting

Customize which items are shown in the mode-line.

#+begin_src emacs-lisp
  (setq-default mode-line-format
                (list " %m: %b, line %l."
                      (mode-line-spacing)
                      '((:eval display-time-string))))
#+end_src

export PS1="[\u@\h \W]\\$ "
export PATH=$PATH:"$HOME"/.bin
export HISTCONTROL=ignoreboth:erasedups
export HISTIGNORE="h:history"
export VISUAL=emacs
export EDITOR=mg

shopt -s autocd
shopt -s cdspell
shopt -s cmdhist
shopt -s histappend
shopt -s histverify
shopt -s checkwinsize

alias cp="cp -iv"
alias mv="mv -iv"
alias rm="rm -Iv"
alias du="du -hc"
alias h="history"
alias mkdir="mkdir -pv"
alias df="df -Th | sort -h"
alias tree="tree --dirsfirst"
alias ll="ls -BlAh --group-directories-first --color=auto"

bind 'set completion-ignore-case on'